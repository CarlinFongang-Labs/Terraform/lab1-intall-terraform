# Terrarm | Installation / configuration de terraform

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
On installera Terraform en récupérant le binaire sur le [site officiel de Terraform](https://developer.hashicorp.com/terraform/install).

On verra comment configurer Terraform sous :
- Microsoft Windows
- Linux

On vérifiera que la configuration de Terraform est effective.

## Description
Terraform est un outil open-source développé par HashiCorp qui permet de créer, gérer et provisionner l'infrastructure de manière déclarative. Il est largement utilisé dans le domaine du cloud computing et de l'automatisation de l'infrastructure. Avec Terraform, vous pouvez décrire votre infrastructure cible dans un fichier de configuration, puis Terraform se charge de créer et de gérer les ressources nécessaires pour atteindre cet état défini.

La spécificité du format d'installation de Terraform est qu'il s'agit d'un fichier binaire exécutable. Cela signifie que Terraform est distribué sous forme de binaire compilé pour différentes plateformes, telles que Windows, Linux et macOS. Cette approche facilite l'installation et la mise à jour de Terraform sur différents systèmes d'exploitation, car il vous suffit de télécharger le fichier binaire correspondant à votre plateforme et de l'exécuter. Il n'est pas nécessaire d'installer Terraform via un gestionnaire de packages ou de le compiler à partir des sources. Cela rend le processus d'installation de Terraform simple et direct pour les utilisateurs.

### 3. Configuration de Terraform sous Windows
Sous Windows :

1. Télécharger le binaire : [Terraform](https://developer.hashicorp.com/terraform/install?product_intent=terraform#windows)
   >![alt text](img/image-3.png)

   Une fois l'archive téléchargée, la décompresser pour l'étape suivante.
   >![alt text](img/image-4.png)

2. Copier le binaire téléchargé dans un dossier dans la partition principale du disque dur :
   - Créer un nouveau répertoire **"Terraform"** dans le disque **"C"**.
     >![alt text](img/image-1.png)
     *Création du répertoire "Terraform"*
   - Ajouter le binaire téléchargé à l'étape précédente.
     >![alt text](img/image-2.png)
     *Ajout du binaire "terraform.exe"*

3. Ajout de Terraform aux variables d'environnement :
    - Ouvrir les propriétés système Windows.
    - Se rendre dans le panneau de configuration Windows.
    >![alt text](img/image-5.png)

    - Ouvrir "Système de sécurité".
    >![alt text](img/image-6.png)

    - Accéder au menu "Système".
    >![alt text](img/image-7.png)

    - Accéder au bouton "Paramètres avancés du système".
    >![alt text](img/image-8.png)

    - Une nouvelle fenêtre s'ouvre :
    >![alt text](img/image-9.png)

    *Fenêtre "Propriétés du système"*
    - Se rendre dans "Variable d'environnement".
    >![alt text](img/image-10.png)

    - Sélectionner la ligne "Path" et cliquer sur **Modifier**.
    >![alt text](img/image-11.png)

    - La fenêtre listant les différentes variables d'environnement s'ouvre.
    >![alt text](img/image-12.png)

    - Cliquer ensuite sur le bouton **Nouveau** pour ajouter le répertoire où se trouve le binaire de Terraform, puis valider.
    >![alt text](img/image-13.png)

    - Tester la configuration de Terraform :
        Depuis un nouveau terminal, exécuter une commande Terraform, par exemple **"terraform version"**.
        >![alt text](img/image-14.png)
        *Version de Terraform installée*

### 4. Configuration de Terraform sous Linux (& MacOS)
L'OS utilisé dans notre cas est CentOS.
>![alt text](img/image-15.png)

- Installation de unzip et wget : ces utilitaires nous permettront respectivement de décompresser et télécharger l'archive contenant le binaire Terraform.

  ```bash
  sudo yum install unzip wget -y
  ```
  >![alt text](img/image-16.png)

- Télécharger Terraform [pour Linux](https://releases.hashicorp.com/terraform/1.7.4/terraform_1.7.4_linux_amd64.zip).

  ```bash
  wget https://releases.hashicorp.com/terraform/1.7.4/terraform_1.7.4_linux_amd64.zip
  ```
  >![alt text](img/image-17.png)

- Décompression de l'archive.


  ```bash
  unzip terraform_1.7.4_linux_amd64.zip
  ```
  >![alt text](img/image-18.png)

- Copie de Terraform vers le répertoire des binaires Linux.
  ```bash
  cp terraform /usr/local/bin
  ```
  >![alt text](img/image-19.png)

- Test de l'installation de Terraform.
  >![alt text](img/image-20.png)


